export const MUNICH_CENTER = [48.1307, 11.5669];
export const MAP_ZOOM = 15;

// Backend API
export const BOOKING_API = '/api/bookings/';
export const SEARCH_PROPERTY_API = '/api/properties/';
export const GEO_LOCATION_API = '/api/geo/';
export const SEARCH_PROPERTY_URL = `${process.env.REACT_APP_BACKEND_API}${SEARCH_PROPERTY_API}`;;
export const BOOKING_URL = `${process.env.REACT_APP_BACKEND_API}${BOOKING_API}`;
export const GEO_LOCATION_URL = `${process.env.REACT_APP_BACKEND_API}${GEO_LOCATION_API}`;

// Google API
export const GOOGLE_MAP_URL = 'https://maps.google.com/maps/api/js';
