import React from "react";
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';

import PlacesPage from './pages/PlacesPage';
import HomePage from './pages/HomePage';
import * as serviceWorker from './serviceWorker';
import { SearchProvider } from './SearchContext';

import './styles/main.scss';


ReactDOM.render(
  <React.StrictMode>
    <SearchProvider>
      <Router>
        <Switch>
            <Route exact path="/">
              <HomePage />
            </Route>
            <Route path="/places">
              <PlacesPage />
            </Route>
          </Switch>
      </Router>
    </SearchProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
