import React from 'react';

import Header from '../components/Header';
import SearchForm from '../components/SearchForm';


function HomePage () {
  return (
    <div>
      <Header />
      <div className="homepage">
        <h1>Find a place to stay for your next destination</h1>
        <SearchForm />
        </div>
    </div>
  );
}

export default HomePage;
