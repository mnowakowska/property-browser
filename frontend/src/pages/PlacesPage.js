import React from 'react';

import PlacesMap from '../components/PlacesMap';
import Header from '../components/Header';


function PlacesPage() {
  return (
    <div>
      <Header />
      <PlacesMap />
    </div>
  );
}

export default PlacesPage;
