import React, { createContext, useState } from "react";

export const SearchContext = createContext();

export const SearchProvider = ({ children }) => {
  const initialLocation = {
    name: '',
    lat: null,
    lng: null
  }
  const [location, setLocation] = useState(initialLocation);
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');

  return (
    <SearchContext.Provider
      value={{
        location,
        startDate,
        endDate,
        setLocation,
        setStartDate,
        setEndDate
      }}
    >
      {children}
    </SearchContext.Provider>
  );
};
