import React, {useState, useEffect, useContext} from 'react';
import axios from 'axios';

import Map from './MapWithMarkers';
import PlacesList from './PlacesList';
import { SearchContext } from '../SearchContext';
import { SEARCH_PROPERTY_URL } from '../conts';


function PlacesMap() {
  const [places, setPlaces] = useState([]);
  const searchContext = useContext(SearchContext);

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        `${SEARCH_PROPERTY_URL}?lat=${searchContext.location.lat}&lng=${searchContext.location.lng}`
      );
      setPlaces(result.data);
    };
    fetchData();
  }, [searchContext.location]);

  return (
    <div>
      <Map places={places} searchContext={searchContext}/>
      <PlacesList places={places} searchContext={searchContext} />
    </div>
  );
}

export default PlacesMap;
