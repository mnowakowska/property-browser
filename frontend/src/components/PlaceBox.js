import React from 'react';

import apartment from '../assets/interior.jpg';
import BookButton from './BookButton';


const PlaceBox = ({ place, searchParams, className }) => (
  <div className={`marker-container ${className}`}>
    <div className='place-box'>
      <div className='photo-container'>
        <img src={apartment} className='photo' alt='apartment-img' />
      </div>
      <div className='description'>
        <div>
          <div className='title'>{place.name}</div>
          <div>{place.vicinity}</div>
        </div>
        <div>
          <div className='price'>{place.price}$</div>
          <div>Designs may vary</div>
        </div>
      </div>
    </div>
    <BookButton place={place} searchParams={searchParams} />
  </div>
);

export default PlaceBox;
