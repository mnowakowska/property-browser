import React, {useContext, useState} from 'react';
import { withRouter } from 'react-router-dom'
import axios from 'axios';

import { SearchContext } from '../SearchContext';
import { GEO_LOCATION_URL } from '../conts';


function SearchForm ({ history }) {
  const searchParams = useContext(SearchContext);
  const [location, setLocation] = useState('');

  const getGeoCoordinates = async (location) => {
    const result = await axios(
      `${GEO_LOCATION_URL}?location=${location}`
    );
    return result.data;
  };

  const handleSubmit = async (evt) => {
      evt.preventDefault();

      const coordinates = await getGeoCoordinates(location);
      if (!coordinates){
        return alert('Incorrect Location. Please enter a valid city');
      }
      searchParams.setLocation({
        name: location,
        lat: coordinates.lat,
        lng: coordinates.lng
      });
      history.push('/places');
  }

  return (
    <form onSubmit={handleSubmit} className='search-form'>
      <div className='input-container'>
        <label>
          Location
        </label>
        <input
          type="text"
          value={location}
          onChange={e => setLocation(e.target.value)}
          required
          name='location'
          className='search-input'
          placeholder='Where do you want to go?'
        />
      </div>
      <div className='input-container'>
        <label>
          Start date
        </label>
        <input
          type="date"
          value={searchParams.startDate}
          onChange={e => searchParams.setStartDate(e.target.value)}
          required
        />
      </div>
      <div className='input-container'>
        <label>
          End date
        </label>
        <input
          type="date"
          value={searchParams.endDate}
          onChange={e => searchParams.setEndDate(e.target.value)}
          required
        />
      </div>
      <div>
        <button type="submit" className='button'>Search</button>
      </div>
    </form>
  );
}

export default withRouter(SearchForm);
