import React from 'react';

import PlaceBox from './PlaceBox';


function PlacesList({ places, searchContext }) {
  return (
    <div className='places-list'>
      <h3>Results for {searchContext.location.name}</h3>
      {
        places.map(
          place => <PlaceBox
            place={place}
            key={place.id}
            searchParams={searchContext}
            className='place-row'
          />
        )
      }
    </div>
  );
}


export default PlacesList;
