import React from 'react';
import axios from 'axios';

import { BOOKING_URL } from '../conts';


export const bookPlace = (place, searchParams) => {
  if (!( searchParams.startDate && searchParams.endDate)) {
    alert('Go to the search page and provide start date and end date');
  }

  axios.post(BOOKING_URL, {
    property_data: {
      place_id: place.place_id,
      name: place.name,
      vicinity: place.vicinity
    },
    start_date: searchParams.startDate,
    end_date: searchParams.endDate,
    price: place.price
  })
  .then(() => {
    alert('Property successfully booked!');
  })
  .catch(error => {
    const message =  Object.values(error.response.data).join(". ")
    alert(`We are unable to book the property. ${message}`);
  });
}


const BookButton = ({ place, searchParams }) => {
  return (
    <button
      id={`button-${place.place_id}`}
      className='button'
      onClick={() => bookPlace(place, searchParams)}
    >
      Book
    </button>
  );
}

export default BookButton;
