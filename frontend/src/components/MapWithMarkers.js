import React from 'react';
import ReactDOMServer from 'react-dom/server';

import HomeIconDefault from '../assets/HomeIconDefault.svg';
import HomeIconActive from '../assets/HomeIconActive.svg';
import Map from './Map';
import PlaceBox from './PlaceBox';
import { bookPlace } from './BookButton';


function MapWithMarkers({ places, searchContext }) {
  let activePlace = {
    marker: null,
    infoWindow: null
  };

  const getInfoWindow = () => {
    return new window.google.maps.InfoWindow({
      content: '',
      maxWidth: 300,
      maxHeight: 300,
    })
  };

  const setActiveMarker = (marker) => {
    activePlace.marker && activePlace.marker.setIcon(HomeIconDefault);
    marker.setIcon(HomeIconActive);
    activePlace.marker = marker;
  }

  const setActiveInfoWindow = (marker, place) => {
    const infoWindow = getInfoWindow();
    const content = ReactDOMServer.renderToString(<PlaceBox place={place} searchContext={searchContext} />);
    infoWindow.setContent(content);
    window.google.maps.event.addListener(infoWindow, 'closeclick',() => {
       marker.setIcon(HomeIconDefault);
    });
    window.google.maps.event.addListener(infoWindow, 'domready', function() {
      window.google.maps.event.addDomListener(document.getElementById(`button-${place.place_id}`), 'click', function() {
          bookPlace(place, searchContext);
        });
    });
    activePlace.infoWindow && activePlace.infoWindow.close();
    activePlace.infoWindow = infoWindow;

    return infoWindow;
  }

  const onMarkerClick = (map, marker, place) => {
    setActiveMarker(marker);
    const infoWindow = setActiveInfoWindow(marker, place);
    infoWindow.open(map, marker);
  }

  const addMarkers = map => {
    places.forEach(place => {
      const marker = new window.google.maps.Marker({
        map,
        position: {lat: place.latitude, lng: place.longitude},
        title: place.name,
        icon: HomeIconDefault
      });
      marker.addListener(`click`, (e) => onMarkerClick(map, marker, place));
    })
  }

  return (
    <Map onMount={addMarkers} location={searchContext.location} />
  );
}

export default MapWithMarkers;
