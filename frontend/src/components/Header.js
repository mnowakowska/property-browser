import React from 'react';

import BurgerIcon from '../assets/BurgerIcon.svg';
import LimehomeLogo from '../assets/LimehomeLogo.svg';


function Header () {
  return (
    <div className='header'>
      <a href='/'>
        <img src={LimehomeLogo} className='logo' alt='logo' />
      </a>
      <img src={BurgerIcon} alt='home' />
    </div>
  );
}

export default Header;
