import React, { useEffect, useRef } from 'react'

import { GOOGLE_MAP_URL, MAP_ZOOM } from '../conts';


function Map({ location, onMount, className }) {
  const mapProps = {ref: useRef(), className};
  const options = {
    center: {lat: location.lat, lng: location.lng},
    zoom: MAP_ZOOM,
  };

  const onLoad = () => {
    const map = new window.google.maps.Map(mapProps.ref.current, options);
    onMount && onMount(map);
  }

  const initializeMap = () => {
    const script = document.createElement(`script`);
    script.type = `text/javascript`;
    script.src = `${GOOGLE_MAP_URL}?key=${process.env.REACT_APP_GOOGLE_API_KEY}`;
    const headScript = document.getElementsByTagName(`script`)[0];
    headScript.parentNode.insertBefore(script, headScript);
    script.addEventListener(`load`, onLoad);
    return () => script.removeEventListener(`load`, onLoad);
  };

  useEffect(() => {
    if (!window.google) {
      initializeMap();
    } else onLoad();
  });

  return (
    <div {...mapProps} style={{height: `30rem`}} />
  )
}


export default Map;
