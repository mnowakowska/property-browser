import React from 'react';
import { shallow } from 'enzyme';

import HomePage from '../pages/HomePage';
import PlacesPage from '../pages/PlacesPage';
import Header from '../components/Header';
import PlacesMap from '../components/PlacesMap';
import SearchForm from '../components/SearchForm';


test('renders homepage with form', () => {
  const wrapper = shallow(<HomePage />);
  expect(wrapper.find('h1')).toHaveLength(1);
  expect(wrapper.find(SearchForm)).toHaveLength(1);
});

test('renders PlacesPage with map', () => {
  const wrapper = shallow(<PlacesPage />);
  expect(wrapper.find(Header)).toHaveLength(1);
  expect(wrapper.find(PlacesMap)).toHaveLength(1);
});
