## API

API to get properties around a specific Latitude and Longitude
-------------------------------------------
`GET` /api/properties/

Query parameters
- `lat`: Latitude
- `lat`: Latitude

Example:
http://localhost:8000/api/properties/?lat=51.1078852&lng=17.0385376

Response:
```json
[
    {
        "name": "Radisson Blu",
        "id": "f590ef02719b750ecb6281ba85fa55e9fdf44c03",
        "place_id": "ChIJpS7t8NfpD0cRYDSud_texYk",
        "vicinity": "10 Purkyniego St, Wrocław",
        "price": 90,
        "latitude": 51.1107642,
        "longitude": 17.0438813
    }
]
```

API to create a booking for a property 
-------------------------------------------
`POST` /api/bookings/

Post payload

```json
{
     property_data: {
        place_id: <property id>,
        name: <property name>,
        vicinity: <property address>
     }
     start_date: <booking start date>,
     end_date: <booking end date>,
     price: <booking price>
}
```


API to return all bookings for a property
-------------------------------------------
`GET` /api/properties/<property_id>/bookings/

Response:
```json
[
    {
        "created_at": "2020-07-12T15:08:05.166232",
        "start_date": "2020-07-11",
        "end_date": "2020-07-30",
        "price": 120.0
    }
]
```

API to return all properties that were booked
-------------------------------------------
`GET` /api/properties/booked/

Response:
```json
[
    {
        "name": "Hotel Motel One München-Sendlinger Tor",
        "place_id": "ChIJqZSRs1jfnUcRcktahygPcPg",
        "vicinity": "Herzog-Wilhelm-Straße 28, München",
        "bookings_url": "http://localhost:8000/api/properties/ChIJqZSRs1jfnUcRcktahygPcPg/bookings/"
    },
    {
        "name": "Absynt Hostel Wrocław",
        "place_id": "ChIJASvzOwvCD0cRxSNHfhODiAA",
        "vicinity": "Świętego Antoniego 15, Wrocław",
        "bookings_url": "http://localhost:8000/api/properties/ChIJASvzOwvCD0cRxSNHfhODiAA/bookings/"
    }
]
```

API to return coordinates for a location name
-------------------------------------------
`GET` /api/geo/

Query parameters
- `location`: <location>

Example:
http://localhost:8000/api/geo/?location=london

Response:
```json
{
    lat: 51.1078852,
    lng: 17.0385376
}
```


API for registering users
-------------------------------------------
`POST` /api/register/


Post Payload 
- `username`: user's username - has to be unique
- `email`: user's email 
- `password`: user's password


API for authenticating
-------------------------------------------
`POST` /auth/login/

Post Payload 
- `username`: user's username
- `password`: user's password

`GET` /auth/logout/
