import random

from django.contrib.auth.models import User
from django.db.models import Q
from rest_framework import serializers

from property.models import Booking, Property


class PropertySerializer(serializers.ModelSerializer):
    bookings_url = serializers.HyperlinkedIdentityField(
        view_name='api:property-bookings',
        lookup_field='place_id',
        read_only=True,
    )

    class Meta:
        model = Property
        fields = ['name', 'place_id', 'vicinity', 'bookings_url']
        extra_kwargs = {
            'place_id': {'validators': []},
        }


class BookingSerializer(serializers.ModelSerializer):
    property_data = PropertySerializer(write_only=True)

    class Meta:
        model = Booking
        fields = ['created_at', 'property_data', 'start_date', 'end_date', 'price']
        read_only_fields = ['created_at']

    def validate(self, data):
        bookings = Booking.objects.filter(
            property=data['property_data']['place_id']
        ).filter(
            Q(start_date__gte=data['start_date'], end_date__lte=data['end_date']) |
            Q(start_date__lte=data['end_date'], end_date__gte=data['end_date']) |
            Q(start_date__lte=data['start_date'], end_date__gte=data['start_date'])
        )
        if bookings.exists():
            raise serializers.ValidationError('The property is booked in these dates')
        return data

    def create(self, validated_data):
        property_data = validated_data.pop('property_data')
        property, _ = Property.objects.get_or_create(
            place_id=property_data['place_id'],
            defaults={
                'name': property_data['name'],
                'vicinity': property_data['vicinity'],
            },
        )
        booking = Booking.objects.create(
            **validated_data, property=property,
        )
        return booking


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ['username', 'email', 'password']

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = User.objects.create(**validated_data)
        user.set_password(password)
        user.save()

        return user


class PropertyListSerializer(serializers.Serializer):
    name = serializers.CharField()
    id = serializers.CharField()
    place_id = serializers.CharField()
    vicinity = serializers.CharField()
    price = serializers.SerializerMethodField()
    latitude = serializers.FloatField(source='geometry.location.lat')
    longitude = serializers.FloatField(source='geometry.location.lng')

    class Meta:
        fields = ['name', 'id', 'place_id', 'vicinity', 'latitude', 'longitude', 'price']

    def get_price(self, obj):
        return random.randint(10, 100)
