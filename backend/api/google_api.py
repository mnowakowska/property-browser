import logging
import requests
from urllib.parse import urlencode

from django.conf import settings

logger = logging.getLogger(__name__)


PLACES_URL = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json'
GEO_URL = 'https://maps.googleapis.com/maps/api/geocode/json'


def get_nearby_places(lat, lng):
    params = {
        'key': settings.GOOGLE_API_KEY,
        'location': f'{lat},{lng}',
        'radius': 1000,
        'type': 'lodging',
    }
    response = requests.get(f'{PLACES_URL}?{urlencode(params)}')
    response = response.json()
    if response['status'] != 'OK':
        logger.error(response.get('error_message'))
    return response['results']


def get_coordinates(location):
    params = {
        'address': location,
        'key': settings.GOOGLE_API_KEY,
    }
    response = requests.get(f'{GEO_URL}?{urlencode(params)}')
    response = response.json()
    result = response['results']
    if len(result) == 0:
        return None
    location = result[0]['geometry']['location']
    return {
        'lat': location['lat'],
        'lng': location['lng'],
    }
