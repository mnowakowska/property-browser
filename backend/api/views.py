from django.contrib.auth.models import User
from rest_framework import generics, mixins, permissions, status, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from property.models import Booking, Property
from api.serializers import (
    BookingSerializer,
    PropertyListSerializer,
    PropertySerializer,
    UserSerializer,
)
from api.google_api import get_nearby_places, get_coordinates


class CreateBookingView(generics.CreateAPIView):
    """ Creates a booking for a property """
    permission_classes = [permissions.AllowAny]
    serializer_class = BookingSerializer
    queryset = Booking.objects.all()


class BookingListView(generics.ListAPIView):
    """ Returns all bookings for a property """
    permission_classes = [permissions.AllowAny]
    serializer_class = BookingSerializer

    def get_queryset(self):
        return Booking.objects.filter(property=self.kwargs['place_id'])


class BookedPropertyListView(generics.ListAPIView):
    """ Returns properties that were booked """
    permission_classes = [permissions.AllowAny]
    serializer_class = PropertySerializer
    queryset = Property.objects.all()


class CreateUserView(generics.CreateAPIView):
    """ API for registering users """
    permission_classes = [permissions.AllowAny]
    queryset = User.objects.all()
    serializer_class = UserSerializer


class PropertyListView(APIView):
    """ Returns properties around Lat/Lon """
    permission_classes = [permissions.AllowAny]
    serializer_class = PropertyListSerializer

    def get(self, request, format=None):
        lat = request.GET.get('lat')
        lng = request.GET.get('lng')
        if not (lat and lng):
            return Response(
                {'errors': ['lat and lng are required query params']},
                status=status.HTTP_400_BAD_REQUEST,
            )
        properties = get_nearby_places(lat, lng)
        report_serializer = PropertyListSerializer(properties, many=True)
        return Response(report_serializer.data)


class GeoCoordinatesApi(APIView):
    """ Returns the coordinates for a location name """
    permission_classes = [permissions.AllowAny]

    def get(self, request, format=None):
        location = request.GET.get('location')
        if not location:
            return Response(
                {'errors': ['location is a required query param']},
                status=status.HTTP_400_BAD_REQUEST,
            )
        coordinates = get_coordinates(location)
        return Response(coordinates)
