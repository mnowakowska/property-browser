from django.urls import include, path, re_path

from api.views import (
    BookedPropertyListView,
    BookingListView,
    CreateBookingView,
    CreateUserView,
    GeoCoordinatesApi,
    PropertyListView,
)

app_name = 'api'


urlpatterns = [
    path('register/', CreateUserView.as_view(), name='users-create'),
    path('properties/', PropertyListView.as_view(), name='property-list'),
    path('geo/', GeoCoordinatesApi.as_view(), name='geo-location'),
    path('properties/booked/', BookedPropertyListView.as_view(), name='booked-property-list'),
    re_path(r'properties/(?P<place_id>[0-9A-Za-z-_]*)/bookings/', BookingListView.as_view(), name='property-bookings'),
    path('bookings/', CreateBookingView.as_view(), name='book'),
]
