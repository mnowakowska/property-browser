from django.contrib.auth.models import User
from django.db import models


class Property(models.Model):
    place_id = models.CharField(max_length=250, primary_key=True)
    name = models.CharField(max_length=250)
    vicinity = models.CharField(max_length=250)


class Booking(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    property = models.ForeignKey(Property, on_delete=models.CASCADE)
    start_date = models.DateField()
    end_date = models.DateField()
    price = models.FloatField()
