from django.contrib import admin

from property.models import Booking, Property

admin.site.register(Booking)
admin.site.register(Property)
