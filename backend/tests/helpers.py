import re


class pytest_regex:
    """Assert that a given string meets some expectations."""

    def __init__(self, pattern):
        self._regex = re.compile(pattern)

    def __eq__(self, actual):
        return bool(self._regex.match(str(actual)))

    def __repr__(self):
        return self._regex.pattern
