from django.urls import reverse
from rest_framework import status

from tests.helpers import pytest_regex


def test_get_properties_no_params(db, api_client, nearby_properties):
    url = reverse('api:property-list')
    response = api_client.get(url)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'errors': ['lat and lng are required query params'],
    }


def test_get_properties(db, api_client, nearby_properties):
    url = reverse('api:property-list')
    response = api_client.get(url, {'lat': 51.5084092, 'lng': -0.1247})
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == [
      {
        'name': 'Amba Hotel Charing Cross',
        'id': 'c6e9dc26feec0f09f381ccd89ecdb4ce3e1ab087',
        'place_id': 'ChIJZf2Jlc4EdkgRklHks51KYBo',
        'vicinity': 'Strand, London',
        'price': pytest_regex(r'\d+'),
        'latitude': 51.5084092,
        'longitude': -0.124743
      },
      {
        'name': 'Corinthia London',
        'id': 'e6fb8cbd3c615754e21ed5a3671a98558997095f',
        'place_id': 'ChIJ0dU6L88EdkgRSRva6TI0lsY',
        'vicinity': 'Whitehall Place, London',
        'price': pytest_regex(r'\d+'),
        'latitude': 51.50674359999999,
        'longitude': -0.1244041
      }
    ]


def test_get_geo_coordinates_no_params(db, api_client, goe_coordinates):
    url = reverse('api:geo-location')
    response = api_client.get(url)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'errors': ['location is a required query param'],
    }


def test_get_geo_coordinates(db, api_client, goe_coordinates):
    url = reverse('api:geo-location')
    response = api_client.get(url, {'location': 'London'})
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {
        'lat': 51.5067435999,
        'lng': -0.12440,
    }


def test_book_property(db, api_client):
    url = reverse('api:book')
    json_data = {
        'property_data': {
            'place_id': 'ChIJ0dU6L88EdkgRSRva6TI0lsY',
            'vicinity': 'Whitehall Place, London',
            'name': 'Corinthia London',
        },
        'start_date': '2020-07-15',
        'end_date': '2020-07-19',
        'price': 99,
    }
    response = api_client.post(url, json_data, format='json')
    assert response.status_code == status.HTTP_201_CREATED


def test_book_property_validation_error(db, api_client):
    url = reverse('api:book')
    json_data = {
        'property_data': {
            'place_id': 'ChIJ0dU6L88EdkgRSRva6TI0lsY',
            'vicinity': 'Whitehall Place, London',
            'name': 'Corinthia London',
        },
        'end_date': '2020-07-19',
        'price': 99,
    }
    response = api_client.post(url, json_data, format='json')
    assert response.json() == {'start_date': ['This field is required.']}
    assert response.status_code == status.HTTP_400_BAD_REQUEST


def test_book_property_validate_dates(db, api_client, booking):
    url = reverse('api:book')
    json_data = {
        'property_data': {
            'place_id': 'ChIJ0dU6L88EdkgRSRva6TI0lsY',
            'vicinity': 'Whitehall Place, London',
            'name': 'Corinthia London',
        },
        'end_date': '2020-07-19',
        'start_date': '2020-07-15',
        'price': 99,
    }
    response = api_client.post(url, json_data, format='json')
    assert response.json() == {'non_field_errors': ['The property is booked in these dates']}
    assert response.status_code == status.HTTP_400_BAD_REQUEST


def test_get_property_bookings(db, api_client, booking, property):
    url = reverse('api:property-bookings', args=[property.place_id])
    response = api_client.get(url)
    assert response.json() == [{
        'created_at': booking.created_at.isoformat(),
        'end_date': '2020-07-16',
        'price': 40.0,
        'start_date': '2020-07-01',
    }]
    assert response.status_code == status.HTTP_200_OK
