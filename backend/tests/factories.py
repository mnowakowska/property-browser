import factory

from django.contrib.auth.models import User

from property.models import Booking, Property


class BookingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Booking


class PropertyFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Property


class UserFactory(factory.django.DjangoModelFactory):
    id = factory.Sequence(lambda x: x)
    username = factory.Sequence(lambda n: 'User %03d' % n)
    email = factory.Sequence(lambda n: 'user%03d@test.com' % n)

    class Meta:
        model = User
