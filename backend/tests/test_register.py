import pytest

from django.urls import reverse
from rest_framework import status


def test_register_user(db, unauthenticated_client):
    url = reverse('api:users-create')
    response = unauthenticated_client.post(url, {'username': 'user1234', 'email': 'user1@test.com', 'password': '1234'})
    assert response.status_code == status.HTTP_201_CREATED
    assert response.json() == {'username': 'user1234', 'email': 'user1@test.com'}


def test_register_user_validation(db, unauthenticated_client, user):
    url = reverse('api:users-create')
    response = unauthenticated_client.post(url, {'username': user.username, 'email': 'user1@test.com', 'password': '1234'})
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert 'A user with that username already exists.' in response.json()['username']
