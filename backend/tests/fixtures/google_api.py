import pytest
from mock import Mock


GOOGLE_NEARBY_PROPERTIES = [
  {
    'business_status': 'OPERATIONAL',
    'geometry': {
      'location': {
        'lat': 51.5084092,
        'lng': -0.124743
      },
      'viewport': {
        'northeast': {
          'lat': 51.50973863029149,
          'lng': -0.123591319708498
        },
        'southwest': {
          'lat': 51.5070406697085,
          'lng': -0.126289280291502
        }
      }
    },
    'icon': 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png',
    'id': 'c6e9dc26feec0f09f381ccd89ecdb4ce3e1ab087',
    'name': 'Amba Hotel Charing Cross',
    'opening_hours': {
      'open_now': True
    },
    'photos': [
      {
        'height': 1365,
        'html_attributions': [
          '<a href="https://maps.google.com/maps/contrib/113257555899388622502">Amba Hotel Charing Cross</a>'
        ],
        'photo_reference': 'CmRaAAAAiwiZrruhNSKj_c10n6ffLroaPCOR4pmPt3ZJjQtr8HDpjELfxHXAsHPleGkKOrTsMK_w4XKEpC9nuxcilNejlO1fzXcrzzSrkvnBJJ3cba5IanfO1NX6vTifAvZsnUrFEhBWRRnCV6Gq8PDCO1QS0punGhSszHa158XUVAFhHgFKJMRgB8xG-Q',
        'width': 2048
      }
    ],
    'place_id': 'ChIJZf2Jlc4EdkgRklHks51KYBo',
    'plus_code': {
      'compound_code': 'GV5G+94 London, UK',
      'global_code': '9C3XGV5G+94'
    },
    'rating': 4.5,
    'reference': 'ChIJZf2Jlc4EdkgRklHks51KYBo',
    'scope': 'GOOGLE',
    'types': [
      'lodging',
      'bar',
      'point_of_interest',
      'establishment'
    ],
    'user_ratings_total': 1657,
    'vicinity': 'Strand, London'
  },
  {
    'business_status': 'OPERATIONAL',
    'geometry': {
      'location': {
        'lat': 51.50674359999999,
        'lng': -0.1244041
      },
      'viewport': {
        'northeast': {
          'lat': 51.5078834802915,
          'lng': -0.1229133197084979
        },
        'southwest': {
          'lat': 51.5051855197085,
          'lng': -0.125611280291502
        }
      }
    },
    'icon': 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png',
    'id': 'e6fb8cbd3c615754e21ed5a3671a98558997095f',
    'name': 'Corinthia London',
    'opening_hours': {
      'open_now': True
    },
    'photos': [
      {
        'height': 968,
        'html_attributions': [
          '<a href="https://maps.google.com/maps/contrib/112225963651159793247">Corinthia London</a>'
        ],
        'photo_reference': 'CmRaAAAALMVg1gUnWZDchy8JdNPg7fHRRLrO6xOQU3faMvnYfVjQ-oLaaEvG6QBVK9mw2U_gzha8VfeoqTBmJMFcPmM3ERz4wCAUZETpJDyqf0claMZqeQxrvIuhFlQpbi0vg75_EhCqu6JbR6J1WkRTavFnDAAQGhQXw309aks_SwrCsg5ULsFA6kANMg',
        'width': 1452
      }
    ],
    'place_id': 'ChIJ0dU6L88EdkgRSRva6TI0lsY',
    'plus_code': {
      'compound_code': 'GV4G+M6 London, UK',
      'global_code': '9C3XGV4G+M6'
    },
    'rating': 4.6,
    'reference': 'ChIJ0dU6L88EdkgRSRva6TI0lsY',
    'scope': 'GOOGLE',
    'types': [
      'lodging',
      'point_of_interest',
      'establishment'
    ],
    'user_ratings_total': 2030,
    'vicinity': 'Whitehall Place, London'
  }
]


@pytest.fixture()
def nearby_properties(monkeypatch):
    _mock = Mock(return_value=GOOGLE_NEARBY_PROPERTIES)
    monkeypatch.setattr('api.views.get_nearby_places', _mock)
    return _mock


@pytest.fixture()
def goe_coordinates(monkeypatch):
    _mock = Mock(return_value={
        'lat': 51.5067435999,
        'lng': -0.12440,
    })
    monkeypatch.setattr('api.views.get_coordinates', _mock)
    return _mock
