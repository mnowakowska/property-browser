import datetime
import pytest

from rest_framework.test import APIClient

from tests.factories import BookingFactory, PropertyFactory, UserFactory


@pytest.fixture()
def user():
    return UserFactory()


@pytest.fixture()
def unauthenticated_client():
    client = APIClient()
    return client


@pytest.fixture()
def api_client(unauthenticated_client, user):
    unauthenticated_client.force_authenticate(user)
    return unauthenticated_client


@pytest.fixture()
def booking(property):
    return BookingFactory(
        property=property,
        end_date=datetime.date(2020, 7, 16),
        start_date=datetime.date(2020, 7, 1),
        price=40,
    )


@pytest.fixture()
def property():
    return PropertyFactory(
        place_id='ChIJ0dU6L88EdkgRSRva6TI0lsY',
        vicinity='Whitehall Place, London',
        name='Corinthia London',
    )
