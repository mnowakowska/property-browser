# Service that shows hotels near a specific location on a map and allows to book them

## To run the application

## Setting up

1. Clone the repository
2. In the root directory run
```bash
cp ./backend/.env_example ./backend/.env
cp ./frontend/.env_example ./frontend/.env
```
3. To run the application:
```bash
docker-compose build && docker-compose up
```

Backend: http://localhost:8000/api/
Frontend: http://localhost:3001


## Tests

```bash
docker-compose run backend pytest
docker-compose run frontend npm run test -- --watchAll=false
```
